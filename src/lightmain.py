import nltk
import requests
import heapq
import pymysql
import boto3
import time
import os
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import stopwords
from dotenv import load_dotenv
from pathlib import Path  # python3 only

env_path = Path('..') / '.env'
load_dotenv(dotenv_path=env_path)

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('ia-project')

CONNECTION = pymysql.connect(host=os.getenv("MYSQL_HOST"),
                             user=os.getenv("MYSQL_USER"),
                             password=os.getenv("MYSQL_PWD"),
                             db=os.getenv("MYSQL_DB"),
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

class Node():
    """A node class for A* Pathfinding"""

    def __init__(self, parent=None, page=None,name=None):
        self.parent = parent
        self.page = page
        self.name = name
        self.categorie= table.get_item(Key={"Page": name})

        self.g = 0
        self.h = 0
        self.h2=0
        self.f = 0

    def __eq__(self, other):
        return self.page == other.page

    def getlinks(self):
        with CONNECTION.cursor() as cursor:
            sql = "select id_target from wiki.nodes where id_source=%s;"
            cursor.execute(sql, (self.page,))
            result = cursor.fetchall()
            result = [f['id_target'] for f in result]
            return result
    def getlinksandNames(self):
        with CONNECTION.cursor() as cursor:
            sql = "select no.id_source, no.id_target, na.names from wiki.nodes no join wiki.names na on no.id_target=na.id " \
                  "where no.id_source=%s and na.names in (select name from wiki.description)" #and no.ID_SOURCE in (select ID_TARGET from wiki.nodes no2 where no2.ID_SOURCE=no.id_target);"
            cursor.execute(sql, (self.page,))
            result = cursor.fetchall()
            return result
    def getDescription(self):
        with CONNECTION.cursor() as cursor:
            sql = "select TEXT from wiki.description where NAME=%s "
            cursor.execute(sql, (self.name,))
            result = cursor.fetchone()
            try:
                result=result['TEXT']
            except TypeError:
                result=""
            return result

def get_links_to(node):
    with CONNECTION.cursor() as cursor:
        sql = "select id_source from wiki.nodes where id_target=%s "
        cursor.execute(sql, (node.page,))
        return [f["id_source"] for f in cursor.fetchall()]

def cosine_similarity(dicA, dicB):
    res=0
    newDicA={}
    newDicB={}
    #on transforme la liste de dictionnaires en dictionnaire
    for dic in dicA:
        newDicA[dic['Name']] = dic['Score']
    for dic in dicB:
        newDicB[dic['Name']] = dic['Score']
    #on transforme les dictionnaire en set pour obtenir les clés
    setA=(set(newDicA))
    setB=(set(newDicB))
    # on parcours les clés communes aux 2 dictionnaires
    for name in setA.intersection(setB):
        # cosine similarity
        valueA=float(newDicA[name])
        valueB=float(newDicB[name])

        res= res+ valueA*valueB
    return res*100000

def IdFromName (id):
    with CONNECTION.cursor() as cursor:
        sql = "select id from wiki.names where names=%s;"
        cursor.execute(sql, (id,))
        result = cursor.fetchone()
        return result.get('id')


def astar(start, end):
    """Returns a list of tuples as a path from the given start to the given end in the given maze"""

    # Create start and end node
    start_node = Node(None, start[0],start[1])
    start_node.g = start_node.h = start_node.f = 0
    end_node = Node(None, end[0],end[1])
    end_node.g = end_node.h = end_node.f = 0



    # Initialize both open and closed list
    open_list = []
    closed_list = []

    # Add the start node
    open_list.append(start_node)
    start_time = time.time()

    stopwords_english = set(stopwords.words("english"))
    tfidf = TfidfVectorizer(stop_words=stopwords_english)
    # Loop until you find the end

    while len(open_list) > 0:

        # Get the current node
        current_node = open_list[0]

        current_index = 0
        for index, item in enumerate(open_list):
            if item.f > current_node.f:
                current_node = item
                current_index = index
        # Pop current off open list, add to closed list
        open_list.pop(current_index)
        closed_list.append(current_node)

        # Found the goal
        if current_node == end_node:
            path = []
            current = current_node
            while current is not None:
                path.append(current.name)
                current = current.parent
            return path[::-1] # Return reversed path
        if len(open_list)>200:
            return [start_node.name,end_node.name,"failed"]
        # Generate children
        children = []
        neighboors=current_node.getlinksandNames()

        for adjacent_page in neighboors: # Adjacent pages


            # Create new node
            new_node = Node(current_node, adjacent_page['id_target'],adjacent_page['names'])

            # Append
            children.append(new_node)

        # Loop through children
        childrenToBeam = []
        for child in children:
            # Child is on the closed list

            if child.name == None:
                continue
            try:
                child.name.encode('ascii')
            except UnicodeEncodeError:
                print
                "it was not a ascii-encoded unicode string",child.name
                continue
            #Create the f, g, and h values
            child.g = current_node.g + 0.1
            #child.h = 0 if (child.page==end_node.page) else 100

            try:
                childCategories = child.categorie['Item']
                endCategorie = end_node.categorie['Item']
            except KeyError:
                print
                "it was not a valid key"
                continue
            heuristique = cosine_similarity(endCategorie['Categories'],childCategories['Categories'])

            child.h=heuristique

           # print (child.name,"'s score : ",child.h)

            # Child is already in the open list
            for open_node in open_list:
                if child == open_node :#and child.g > open_node.g:
                    continue

            # Add the child to the list with all children we're going to beam
            if child not in open_list and child not in closed_list:
                childrenToBeam.append(child)
        # sort children based on best heuristic
        childrenToBeam.sort(key=lambda x: x.h, reverse=True)
        # keeping top 5 children
        top10children=childrenToBeam[:20]
        totfidf=[]
        totfidf.append(end_node.getDescription())



        for chilf in open_list:
            totfidf.append(child.getDescription())
        for child in top10children:
            totfidf.append(child.getDescription())
            # add the top 5 children to the open list
            open_list.append(child)
        vecs = tfidf.fit_transform(totfidf)
        corr_matrix = ((vecs * vecs.T).A)
        i=1
        for child in open_list:
            val = corr_matrix[0][i]
            i+=1
            child.h2=val
            child.f =  child.h2 #- child.g



def main():
    startPage ="404"
    badPage = "France"
    finishPage = "The Beatles"


    startId =IdFromName(startPage)
    badId=IdFromName(badPage)
    finishId= IdFromName(finishPage)
    startNode = Node(None, startId,startPage)
    badNode = Node(None, badId,badPage)
    finishNode = Node(None, finishId,finishPage)
    path = astar([startId,startPage], [finishId,finishPage])
    print(path)

if __name__ == '__main__':
    main()
