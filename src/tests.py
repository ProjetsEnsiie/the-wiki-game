from lightmain import *
import multiprocessing
import signal, time, random
import time
import random
random.seed(23)
starttime=time.time()
times=[]

GEOGRAPHY = "France,Britain,China,Seattle,Sao Paulo,Pacific Ocean,Philippines,Sahara,Kenya,Melbourne".split(",")
OBJECT = "Pen,Computer,Cable,Power line,Window,Kitchen,Bag,Paper,Eraser".split(",")
PERSONALITY = "Barack Obama,Kanye West,Elton John,Hitler,Jesus".split(",")
FICTION = "Batman,Harry Potter,Mickey,Doctor Who,Dark Vador".split(",")

def worker(i,return_dict,start,end):
    path = astar(start,end)
    return_dict[i] =path

def randomTest():
        badPage = "France"
        finishPage = "The Beatles"
        words="Facebook,YouTube,404,United States,Donald Trump,Search,Google,Barack Obama,India,World War II,Michael Jackson,United Kingdom,Lady Gaga," \
              "Eminem,The Beatles,Adolf Hitler,Sex,Elizabeth II,Malware,Cristiano Ronaldo,Justin Bieber,World War I," \
              "The Big Bang Theory,Steve Jobs,Canada,Darth Vader,Australia,Stephen Hawking,Lil Wayne,Lionel Messi,List of Presidents of the United States," \
              "How I Met Your Mother,Freddie Mercury,Star Wars,Miley Cyrus,Harry Potter,China,Japan,Germany,Rihanna,Abraham Lincoln," \
              "New York City,Russia,Johnny Depp,Albert Einstein,Kanye West,Tupac Shakur,France,Michael Jordan,Breaking Bad," \
              "Leonardo DiCaprio,Angelina Jolie,Earth"
        wordarray= words.split(",")
        manager = multiprocessing.Manager()
        return_dict = manager.dict()
        jobs = []

        for i in range (5):
            startPage = wordarray[random.randint(0,50)]
            finishPage = wordarray[random.randint(0,50)]
            print("Start: ",startPage,", End: ",finishPage)
            startId =IdFromName(startPage)
            finishId= IdFromName(finishPage)
            startNode = Node(None, startId,startPage)
            finishNode = Node(None, finishId,finishPage)
            p = multiprocessing.Process(target=worker, name="Foo", args=(i,return_dict,[startId,startPage], [finishId,finishPage]))
            jobs.append(p)
            p.start()

            # p.join(60)
            # if p.is_alive():
            #     p.terminate()

            # Cleanup
        for proc in jobs:
                proc.join()
        print (return_dict.values())


def testSequence():
    pages = [("World War I", "Friends"),
             ("Star Wars", "Screen"),
             ("Pen", "Albert Einstein"),
             ("China", "Malware"),
             ("List of Presidents of the United States", "Earth")]

    for (startPage, finishPage) in pages:
        print("Searching path between", startPage, finishPage)
        startId =IdFromName(startPage)
        finishId= IdFromName(finishPage)
        startNode = Node(None, startId,startPage)
        finishNode = Node(None, finishId,finishPage)
        print(astar([startId,startPage], [finishId,finishPage]))


def testOnePage(startPage, finishPage):
    print("Searching path between", startPage, finishPage)
    startId =IdFromName(startPage)
    finishId= IdFromName(finishPage)
    startNode = Node(None, startId,startPage)
    finishNode = Node(None, finishId,finishPage)
    print(astar([startId,startPage], [finishId,finishPage]))


def testSequence():
    pages = "France, Britain, China, New York, Sao Paulo, Pacific Ocean, Philippines, Sahara, Kenya, Melbourne"

    for (startPage, finishPage) in pages:
        print("Searching path between", startPage, finishPage)
        startId =IdFromName(startPage)
        finishId= IdFromName(finishPage)
        startNode = Node(None, startId,startPage)
        finishNode = Node(None, finishId,finishPage)
        print(astar([startId,startPage], [finishId,finishPage]))


def randomSet(list_pages):
    for i in range(3):
        startPage = random.choice(list_pages)
        list_pages.pop(list_pages.index(startPage))
        finishPage = random.choice(list_pages)
        print("From {} to {}".format(startPage, finishPage))
        startId = IdFromName(startPage)
        finishId = IdFromName(finishPage)
        print(astar([startId, startPage], [finishId, finishPage]))

def demo():
    while (True):
        startPage = input("Page de début : ")
        finishPage = input("Page de fin : ")
        startId =IdFromName(startPage)
        finishId= IdFromName(finishPage)
        print(astar([startId,startPage], [finishId,finishPage]))

if __name__ == '__main__':
    demo()
