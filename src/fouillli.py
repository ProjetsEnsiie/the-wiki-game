import json
with open("C:\\Users\\Theo\\Desktop\\wikidata.ttl", "r", encoding="utf-8") as r:
    with open("C:\\Users\\Theo\\Desktop\\out.json", "w") as w:
        builder = []
        current_name = ""
        corrent_topic = ""
        for line in r:
            if '"' in line:
                current_name = line.split('"')[1]
                builder.append(dict(Page=current_name, Categories=[]))
            elif 'partOfTopic' in line:
                current_topic = line.split(":")[2][:-2].strip()
                builder[len(builder)-1]['Categories'].append(dict(Name=current_topic))
            elif 'pagerankScore' in line:
                builder[len(builder)-1]['Categories'][len(builder[len(builder)-1]['Categories'])-1]["Score"] = line.split("Score")[1].replace(" ","").replace("\t","").replace("\n","")
        json.dump(builder, w)