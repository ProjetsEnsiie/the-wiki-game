import pymysql.cursors
from pymongo import MongoClient
import time
from dotenv import load_dotenv
from pathlib import Path  # python3 only


env_path = Path('..') / '.env'
load_dotenv(dotenv_path=env_path)
# Connect to the database
connection = pymysql.connect(host=os.getenv("MYSQL_HOST"),
                             user=os.getenv("MYSQL_USER"),
                             password=os.getenv("MYSQL_PWD"),
                             db=os.getenv("MYSQL_DB"),
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)
client = MongoClient('mongodb://localhost:27017/')

try:
    start_time = time.time()

    with connection.cursor() as cursor:
        # Read a single record
        sql = "select * from wiki.names where names=%s limit 200;"
        cursor.execute(sql, ("Strasbourg",))
        result = cursor.fetchone()
        print(result['ID'])
        sql = "SELECT wiki.description.name FROM wiki.nodes WHERE id_source=608623 " \
              "INNER JOIN wiki.description ON wiki.nodes.id_target = wiki.description.iddescription;"
        cursor.execute(sql)
        print(result)
    collection = client['ia-wikipedia'].ia.find({"Page": ""})
    print(collection)
    print("--- %s seconds ---" % (time.time() - start_time))

finally:
    connection.close()
