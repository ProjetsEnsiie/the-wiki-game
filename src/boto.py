
import boto3
import time
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('ia-project')
print(table.item_count)
start_time = time.time()

response = table.get_item(
    Key={
        "Page": "Annie Chapman"
    }
)
itemAnnie = response['Item']
response = table.get_item(
    Key={
        "Page": "Strasbourg"
    }
)
itemStrasbourg = response['Item']
string="Lille"
response = table.get_item(
    Key={"Page": string}
)
itemLille = response['Item']
print("--- %s seconds ---" % (time.time() - start_time))
start_time = time.time()

def cosine_similarity(dicA, dicB):
    res=0
    newDicA={}
    newDicB={}
    #on transforme la liste de dictionnaires en dictionnaire
    for dic in dicA:
        newDicA[dic['Name']]=dic['Score']
    for dic in dicB:
        newDicB[dic['Name']] = dic['Score']
    #on transforme les dictionnaire en set pour obtenir les clés
    setA=(set(newDicA))
    setB=(set(newDicB))
    # on parcours les clés communes aux 2 dictionnaires
    for name in setA.intersection(setB):
        # cosine similarity
        res= res+ (float(newDicA[name])* float(newDicB[name]))
    return res


print(itemAnnie['Categories'])
print(itemStrasbourg['Categories'])
print(itemLille['Categories'])

print (cosine_similarity(itemAnnie['Categories'],itemStrasbourg['Categories']))
print (cosine_similarity(itemLille['Categories'],itemStrasbourg['Categories']))
print("--- %s seconds ---" % (time.time() - start_time))
